# -*- coding: utf-8 -*-
"""
Created on Thu Oct 12 13:00:11 2023

For Example walkthrough of survey weights lightning talk.


@author: Grant
"""

import numpy as np
import pandas as pd
import statsmodels.formula.api as sm
from sklearn import linear_model
lm=linear_model.LinearRegression()
import matplotlib.pyplot as plot
import seaborn
seaborn.set(style="whitegrid") ##post-rendering fix-up to add gridlines to plots
import os

os.chdir("F:/CRDCN/dli_2023")

## Read in data, generate a sum weight to normalize, and replace missing in income variable - census

data = pd.read_csv( "Census2016.csv" )
data.loc[data['TOTINC']>=88888888,'TOTINC']= pd.NA
obs=len(data)
sumwt=data['WEIGHT'].sum()

##Create a scaled weight variable for calculating proportions etc..
data['WEIGHTSC']=data['WEIGHT']/sumwt

## Compute averages: weighted and unweighted - DIFFERENCE OF $10

uwavg=data['TOTINC'].sum()/obs
wavg=(data['TOTINC']*data['WEIGHTSC']).sum()

print ("Weighted average", wavg)
print ("Unweighted average", uwavg)

## Compute a table: weighted and unweighted - Minimal difference

uwtab=data['AGEGRP'].value_counts().sort_index()
uwtab=uwtab/obs

wtab=data.groupby('AGEGRP')['WEIGHTSC'].sum()



print ("Weighted proportions - Age", wtab)
print ("Unweighted proportions - Age", uwtab)

## Create a histogram for the weight variable to demonstrate why weighting here just isn't that impactful
plot.hist(data['WEIGHT'], bins=5, edgecolor='black')  # Adjust the number of bins as needed
plot.title('Histogram of Census Weights')
plot.xlabel('Weight value')
plot.ylabel('Frequency')
plot.show()

##Load up CCHS where stratified sample exists
data2=pd.read_stata("cchs2011.dta")

obs2=len(data2)
sumwt2=data2['wts_m'].sum()

##Create a scaled weight value for proportions
data2['wts_msc']=data2['wts_m']/sumwt2

uwavg2=data2['inc_3'].sum()/obs2
wavg2=(data2['wts_msc']*data2['inc_3']).sum()

print ("CCHS weighted average", wavg2)
print ("CCHS unweighted average", uwavg2)


#Create some age bins and labels and bin the ages, replace non-response for health status
data2['gen_01']=data2['gen_01'].replace([7,8], pd.NA)
bins=[10,20,30,40,50,60,70,80, float('inf')]
bin_labels=['10-19','20-29','30-39','40-49','50-59','60-69','70-79','80+']

data2['agegrp']=pd.cut(data2['dhh_age'], bins=bins, labels=bin_labels, right=False)

wtab2=data2.groupby('agegrp')['wts_msc'].sum()
uwtab2=data2['agegrp'].value_counts().sort_index()
uwtab2=uwtab2/obs2

print ("CCHS weighted proportions - Age", wtab2)
print ("CCHS unweighted proportions - Age", uwtab2)

data2=data2.dropna(subset=['inc_3', 'dhh_age', 'gen_01'])

## Create a histogram for the weight variable to demonstrate why weighting here matters a good deal more
plot.hist(data2['wts_m'], bins=10, edgecolor='black')  # Adjust the number of bins as needed
plot.title('Histogram of CCHS Weights')
plot.xlabel('Weight value')
plot.ylabel('Frequency')
plot.show()

data3=data2[(data2['wts_m']<5000)]
plot.hist(data3['wts_m'], bins=25, edgecolor='black')  # Adjust the number of bins as needed
plot.title('Histogram of CCHS Weights|Weights<5000')
plot.xlabel('Weight value')
plot.ylabel('Frequency')
plot.show()

#Regression model - unweighted
regresult=sm.ols('inc_3 ~ dhh_age + C(gen_01)', data=data2).fit()
print(regresult.summary())

#Regression model - weighted
regresultwt=sm.glm('inc_3 ~ dhh_age + C(gen_01)', data=data2, freq_weights=np.asarray(data2['wts_m']))
regresultwtfit=regresultwt.fit()
print(regresultwtfit.summary())





