** Stata code for DLI_2023 project:
version 17

//Census component

import delimited "F:\CRDCN\dli_2023\Census2016.csv", clear 

//replace missing data for income
replace totinc=. if totinc==99999999 | totinc==88888888
**NB for whatever reason, this differs from the python and R calculations where some values of totinc are negative, which dramatically decreases the average. The raw data has the negative values. I'm not interested in looking into this for the purposes of this demonstration

//setup the survey architecture in Stata (don't bother with the bootstrap rep weights)
svyset _n [pweight=weight]

//un-weighted income
mean totinc

//weighted income
svy: mean totinc

//unweighted binned ages
tab agegrp

//weighted binned ages
svy: tab agegrp


****************
//CCHS component
****************

use "F:\CRDCN\dli_2023\cchs2011.dta", clear

//Created binned age variable
gen agebinned=1
replace agebinned=2 if dhh_age>19
replace agebinned=3 if dhh_age>29
replace agebinned=4 if dhh_age>39
replace agebinned=5 if dhh_age>49
replace agebinned=6 if dhh_age>59
replace agebinned=7 if dhh_age>69
replace agebinned=8 if dhh_age>79

//replace missing values of self-rated health
replace gen_01=. if gen_01==8 | gen_01==7

//setup the survey architecture in Stata
svyset _n [pweight=wts_m]

//un-weighted income
mean inc_3

//weighted income
svy: mean inc_3

//un-weighted binned ages
tab agebinned

//weighted binned ages
svy: tab agebinned

//un-weighted regression
reg inc_3 i.gen_01 dhh_age

//weighted regression
svy: reg inc_3 i.gen_01 dhh_age